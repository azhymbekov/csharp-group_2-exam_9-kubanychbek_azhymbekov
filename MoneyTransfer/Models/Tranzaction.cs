﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Tranzaction
    {
        public int Id { get; set; }


        public User Contragent { get; set; }
        public string ContragentId { get; set; }


        public DateTime Date { get; set; }
        public double Sum { get; set; }


        public string UserId { get; set; }
        public User User { get; set; }
    }
}
