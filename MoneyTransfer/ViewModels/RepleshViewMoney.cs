﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class RepleshViewMoney
    {
        [Required]
      
        [Remote(action: "CheckPersonalId", controller: "Validation", ErrorMessage = "Такого лиц счета нет")]
        public int PersonalId { get; set; }
        [Required]
       
        [Range(1, 10000, ErrorMessage = "Недопустимое значение")]
        public double Money { get; set; }
    }
}
