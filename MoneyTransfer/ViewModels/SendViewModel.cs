﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class SendViewModel
    {

        [Required]
       
        [Remote(action: "CheckPersonalId", controller: "Validation", ErrorMessage = "Такого лиц счета нет")]
        public int PersonalId { get; set; }
        [Required]
        [Display(Name = "Введи сумму денег")]
        [Remote(action: "CheckCash", controller: "Validation", ErrorMessage = "У вас недостаточно средств")]
        [Range(1, 10000, ErrorMessage = "Недопустимое значение")]
        public double Money { get; set; }
    }
}
