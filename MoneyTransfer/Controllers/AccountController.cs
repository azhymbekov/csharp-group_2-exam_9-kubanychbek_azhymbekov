﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;
using MoneyTransfer.Repositories;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class AccountController : Controller
    {
        
            private readonly UserManager<User> _userManager;
            private readonly SignInManager<User> _signInManager;
            public readonly IAccountService accountService;
            private readonly ApplicationContext context;

            public AccountController(UserManager<User> userManager, IAccountService accountService, SignInManager<User> signInManager, ApplicationContext context)
            {
                _userManager = userManager;
                _signInManager = signInManager;
                this.context = context;
            this.accountService = accountService;
            }



            [HttpGet]
            public IActionResult Register()
            {
                return View();
            }
            [HttpPost]
            public async Task<IActionResult> Register(RegisterViewModel model)
            {
                if (ModelState.IsValid)
                {
                    User user = new User { Email = model.Email, UserName = model.Email, Balance = 1000, PersonalAccount = accountService.GetAccount() };
                    // добавляем пользователя
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        // установка куки
                        await _signInManager.SignInAsync(user, false);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
                return View(model);
            }

            [HttpGet]
            public IActionResult Login(string returnUrl = null)
            {
                return View(new LoginViewModel { ReturnUrl = returnUrl });
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            public async Task<IActionResult> Login(LoginViewModel model)
            {
                if (ModelState.IsValid)
                {
                    User user = context.Users.FirstOrDefault(u => u.PersonalAccount == model.INN);
                    if (user != null)
                    {
                        var result =
                        await _signInManager.PasswordSignInAsync(user?.Email, model.Password, model.RememberMe, false);
                        if (result.Succeeded)
                        {
                            // проверяем, принадлежит ли URL приложению
                            if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                            {
                                return Redirect(model.ReturnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Home");
                            }
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                    }
                }
                return View(model);
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            public async Task<IActionResult> LogOff()
            {
                // удаляем аутентификационные куки
                await _signInManager.SignOutAsync();
                return RedirectToAction("Index", "Home");
            }

        }
    
}
