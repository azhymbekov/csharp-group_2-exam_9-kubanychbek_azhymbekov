﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class TransferController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly ApplicationContext context;

        public TransferController(UserManager<User> userManager, ApplicationContext context)
        {
            _userManager = userManager;
            this.context = context;

        }

        // GET: Transfer
        public ActionResult Index()
        {
            return View();
        }


        [Authorize]
        [HttpGet]
        public ActionResult Send()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Send(SendViewModel model)
        {
            User user = context.Users.FirstOrDefault(u => u.PersonalAccount == model.PersonalId);
            user.Balance += model.Money;
            User kuba = await _userManager.FindByEmailAsync(User.Identity.Name);
            kuba.Balance -= model.Money;
            Tranzaction tranzaction = new Tranzaction()
            {
                Date = DateTime.Now,
                UserId = user.Id,
                ContragentId = kuba.Id,
                Sum = model.Money
            };
            context.Tranzactions.Add(tranzaction);
            context.SaveChanges();


            return RedirectToAction("Index", "Home");
        }

       

        

        [HttpGet]
        public ActionResult Replenish()
        {
            return View();
        }


        [HttpPost]
        public IActionResult Replenish(RepleshViewMoney model)
        {

            User user = context.Users.FirstOrDefault(u => u.PersonalAccount == model.PersonalId);
            user.Balance += model.Money;
            context.SaveChanges();
            return View();
        }


        [Authorize]
        public async Task<IActionResult> Story()
        {
            User kuba = await _userManager.FindByEmailAsync(User.Identity.Name);
            List<Tranzaction> tranzactions = context.Tranzactions.Include(u => u.Contragent).Include(u => u.User)
                .Where(t => t.ContragentId == kuba.Id || t.UserId == kuba.Id).ToList();
            return View(tranzactions);

        }



    }
}