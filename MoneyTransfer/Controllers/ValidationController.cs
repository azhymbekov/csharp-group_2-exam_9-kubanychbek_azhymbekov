﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;

namespace MoneyTransfer.Controllers
{
    public class ValidationController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly ApplicationContext context;

        public ValidationController(UserManager<User> userManager, ApplicationContext context)
        {
            _userManager = userManager;
            this.context = context;

        }
        public bool CheckPersonalId(int PersonalId)
        {
            List<User> users = context.Users.Where(u => u.PersonalAccount == PersonalId).ToList();
            if (users.Any())
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public async Task<bool> CheckCash(int money)
        {
            User user = await _userManager.FindByEmailAsync(User.Identity.Name);
            if (user.Balance > money)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}