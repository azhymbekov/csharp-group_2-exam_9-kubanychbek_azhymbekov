﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using MoneyTransfer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        T GetEntityById(int id);
        IEnumerable<T> GetAllEntities();
        Task<T> AddEntity(T entity);
       
    }

    public class Repository<T> : IRepository<T> where T : Entity
    {
        private readonly ApplicationContext context;

        protected DbSet<T> AllEntities { get; set; }

        public Repository(ApplicationContext context)
        {
            this.context = context;
        }

        public virtual T GetEntityById(int id)
        {
            try
            {
                return AllEntities.First(e => e.Id == id);
            }
            catch (InvalidOperationException ex)
            {
                throw ex;
            }
        }

        public virtual IEnumerable<T> GetAllEntities()
        {
            return AllEntities.ToList();
        }

        public async Task<T> AddEntity(T entity)
        {
            EntityEntry<T> addedEntity = AllEntities.Add(entity);
            await context.SaveChangesAsync();
            return addedEntity.Entity;
        }

       
    }
}
