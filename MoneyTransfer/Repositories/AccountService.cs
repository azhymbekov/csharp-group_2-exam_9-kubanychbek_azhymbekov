﻿using MoneyTransfer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Repositories
{
    public interface IAccountService : IURepository<User>
    {
        int GetAccount();
    }
    public class AccountService : URepository<User>, IAccountService
    {       
            public AccountService(ApplicationContext context)
            :base(context)
            {
            AllEntities = context.Users;
            }
        
            public int GetAccount()
            {
                Random rnd = new Random();
                int value = rnd.Next(100000, 999999);
                List<User> users = AllEntities.Where(u => u.PersonalAccount == value).ToList();
                if (users.Any())
                {
                    return GetAccount();
                }
                return value;
            }
    }
}



