﻿using Microsoft.AspNetCore.Identity;
using MoneyTransfer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Repositories
{


    public interface ITransferService : IURepository<User>
    {
        bool CheckPersonalId(int a);
    }


    public class TransferService : URepository<User>, ITransferService
    {
       
        public TransferService(ApplicationContext context)
             : base(context)
        {
            AllEntities = context.Users;
        }


        public bool CheckPersonalId(int PersonalId)
        {
            List<User> users = AllEntities.Where(u => u.PersonalAccount == PersonalId).ToList();
            if (users.Any())
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
