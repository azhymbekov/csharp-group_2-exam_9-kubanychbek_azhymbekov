﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Repositories
{
    public interface IURepository<T> where T : IdentityUser
    {
    }

    public class URepository<T> : IURepository<T> where T : IdentityUser
    {
        private readonly ApplicationContext context;

        protected DbSet<T> AllEntities { get; set; }

        public URepository(ApplicationContext context)
        {
            this.context = context;
        }
    }
}
