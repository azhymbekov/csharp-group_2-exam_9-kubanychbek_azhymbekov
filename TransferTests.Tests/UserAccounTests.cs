﻿using MoneyTransfer.Models;
using MoneyTransfer.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace TransferTests.Tests
{
    public class UserAccounTests
    {
        private ApplicationContext context = new ApplicationContext(@"Server=(localdb)\mssqllocaldb;Database=MoneyTransferdb;Trusted_Connection=True;MultipleActiveResultSets=true");


        [Fact]
        public void GetAccountMethod()
        {
            // Arrange
            var mock = new Mock<IAccountService>();
            mock.Setup(repo => repo.GetAccount()).Returns(GetTestAccount());


            // Act
            var result = mock.Object.GetAccount();
            User user = context.Users.FirstOrDefault(u => u.PersonalAccount == result);


            // Assert                        
            Assert.Equal(6, result.ToString().Length);
            Assert.Null(user);

        }

        private int GetTestAccount()
        {
            return 123456;
        }

        [Fact]
        public void CheckUserAccount()
        {
            // Arrange
            var mock = new Mock<ITransferService>();
            mock.Setup(repo => repo.CheckPersonalId(It.IsAny<int>()));

            // Act
            var result = mock.Object.CheckPersonalId(454568);
            User user = context.Users.FirstOrDefault(u => u.PersonalAccount == Convert.ToInt32(result));

            Assert.Null(user);
        }

    }
}
